﻿using ArbreNaire_Library;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ArbreNaire_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            ArbreAutoCompletion arbreAutoCompletion = new ArbreAutoCompletion();
            arbreAutoCompletion.AjouterMot("bonjour");

            string nomFichierDictionnaire = "dictionnaire.txt";
            ArbreAutoCompletion arbreAutoCompletion2 = InsererMotsFichier(nomFichierDictionnaire);

            List<string> listeMots = arbreAutoCompletion2.CompleterPrefixe("bo");
            Console.Out.WriteLine(String.Join("\r\n", listeMots));

            ArbreExpression arbreExpression = GenerateurArbreExpression.ExempleExpression2();
            Console.WriteLine(arbreExpression.Racine.Calculer());
            ArbreExpression arbreExpressionGeneree = GenerateurArbreExpression.GenererArbreExpressionPrefixeValide("- + * 42 3 17 / 23 7");
            Console.WriteLine(arbreExpressionGeneree.Racine.Calculer());
        }
        public static ArbreAutoCompletion InsererMotsFichier(string p_nomFichier)
        {
            if(p_nomFichier == null || p_nomFichier.Length == 0)
            {
                throw new ArgumentException(nameof(p_nomFichier));
            }

            string dossierSolution = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.Parent?.FullName;
            string cheminAbsoluFichier = dossierSolution + "\\" + p_nomFichier;

            if (!File.Exists(cheminAbsoluFichier))
            {
                throw new ArgumentException("Le fichier est inaccessible ou n'existe pas.");
            }

            string[] lignes = File.ReadAllLines(cheminAbsoluFichier);


            lignes.Where(lignes => lignes.Length != 0)
                  .Select(mot => string.Join("", mot.Normalize()
                  .ToLower()
                  .Where(caractere => !Char.IsWhiteSpace(caractere))))
                  .Select(mot => string.Join("", mot.Where(Char.IsLetter)))
                  .OrderBy(mot => mot)
                  .GroupBy(mot => mot)
                  .ToArray();

            ArbreAutoCompletion arbreAutoCompletion = new ArbreAutoCompletion();

            foreach(string ligne in lignes)
            {
                arbreAutoCompletion.AjouterMot(ligne);
            }

            return arbreAutoCompletion;
        }
    }
}
