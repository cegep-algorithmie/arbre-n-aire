﻿using System;
using System.Collections.Generic;

namespace ArbreNaire_Library
{
    //Exercice 1
    public class NoeudArbre<TypeElement>
    {
        public List<NoeudArbre<TypeElement>> Enfants { get; set; }
        public TypeElement Valeur { get; set; }

        public NoeudArbre(TypeElement p_valeur)
        {
            this.Enfants = new List<NoeudArbre<TypeElement>>();
            this.Valeur = p_valeur;
        }
    }
}
