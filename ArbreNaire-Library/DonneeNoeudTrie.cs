﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreNaire_Library
{
    //Exercice 2
    public class DonneeNoeudTrie
    {
        public char Caractere { get; set; }
        public string MotEnCours { get; set; }
        public bool Valide { get; set; }
        public DonneeNoeudTrie(char p_caractere, string p_motEnCours, bool p_valide)
        {
            this.Caractere = p_caractere;
            this.MotEnCours = p_motEnCours;
            this.Valide = p_valide;
        }
    }
}
