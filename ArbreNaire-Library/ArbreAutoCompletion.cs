﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreNaire_Library
{
    //Exercice 2
    public class ArbreAutoCompletion : Arbre<DonneeNoeudTrie>
    {
        public ArbreAutoCompletion()
        {
            this.Racine = new NoeudArbre<DonneeNoeudTrie>(new DonneeNoeudTrie(' ', "", false));
        }
        private NoeudArbre<DonneeNoeudTrie> ObtenirNoeudEnfant(NoeudArbre<DonneeNoeudTrie> p_noeud, char p_lettre)
        {
            return p_noeud.Enfants.Where(NoeudArbre => NoeudArbre.Valeur.Caractere == p_lettre).SingleOrDefault();
        }
        public void AjouterMot(string p_mot)
        {
            AjouterMot_rec(p_mot, this.Racine);
        }
        private void AjouterMot_rec(string p_MotEnCours, NoeudArbre<DonneeNoeudTrie> p_noeudEnCours)
        {
            char lettreEncours = p_MotEnCours[0];
            string lettresSuivantes = p_MotEnCours.Substring(1);
            bool estValide = false;

            NoeudArbre<DonneeNoeudTrie> noeudEnfant = ObtenirNoeudEnfant(p_noeudEnCours, lettreEncours);

            if (p_MotEnCours.Length == 1)
            {
                estValide = true;
            }

            if (noeudEnfant == null)
            {
                noeudEnfant = new NoeudArbre<DonneeNoeudTrie>(new DonneeNoeudTrie(lettreEncours, p_noeudEnCours.Valeur.MotEnCours + lettreEncours, estValide));
                p_noeudEnCours.Enfants.Add(noeudEnfant);
            }

            if (estValide == true)
            {
                noeudEnfant.Valeur.Valide = true;
            }
            else
            {
                AjouterMot_rec(lettresSuivantes, noeudEnfant);
            }
        }
        public List<string> CompleterPrefixe(string p_prefixe)
        {
            List<string> listeMots = new List<string>();
            NoeudArbre<DonneeNoeudTrie> noeudArbre = this.RechercherNoeudPrefixe(p_prefixe);

            if(noeudArbre != null)
            {
                listeMots = this.CollecterMots(noeudArbre);
            }

            return listeMots;
        }
        public NoeudArbre<DonneeNoeudTrie> RechercherNoeudPrefixe(string p_prefixe)
        {
            return RechercherNoeudPrefixe_rec(this.Racine, p_prefixe); 
        }
        private NoeudArbre<DonneeNoeudTrie> RechercherNoeudPrefixe_rec(NoeudArbre<DonneeNoeudTrie> p_noeud, string p_prefixe)
        {
            char lettreEnCours = p_prefixe[0];
            string lettresSuivantes = p_prefixe.Substring(1);

            NoeudArbre<DonneeNoeudTrie> noeudEnfant = ObtenirNoeudEnfant(p_noeud, lettreEnCours);
            
            if (noeudEnfant != null && lettresSuivantes.Length > 0)
            {
                noeudEnfant = this.RechercherNoeudPrefixe_rec(noeudEnfant, lettresSuivantes);
            }

            return noeudEnfant;
        }
        private List<string> CollecterMots(NoeudArbre<DonneeNoeudTrie> p_noeud)
        {
            List<string> listeMots = new List<string>();

            if (p_noeud.Valeur.Valide == true)
            {
                listeMots.Add(p_noeud.Valeur.MotEnCours);
            }

            foreach (NoeudArbre<DonneeNoeudTrie> enfant in p_noeud.Enfants)
            {
                listeMots.AddRange(this.CollecterMots(enfant));
            }

            return listeMots;
        }
    }
}
