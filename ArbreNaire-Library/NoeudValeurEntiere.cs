﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreNaire_Library
{
    public class NoeudValeurEntiere : NoeudArbreExpression
    {
        public int Valeur { get; set; }
        public int Calculer()
        {
            return this.Valeur;
        }
    }
}
