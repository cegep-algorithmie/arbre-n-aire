﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreNaire_Library
{
    public abstract class NoeudOperateur : NoeudArbreExpression
    {
        public NoeudArbreExpression Gauche { get; set; }
        public NoeudArbreExpression Droit { get; set; }
        public abstract Func<int, int, int> Valeur { get; }
        public int Calculer()
        {
            return this.Valeur(this.Gauche.Calculer(), this.Droit.Calculer());
        }
    }
}
