﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreNaire_Library
{
    public class GenerateurArbreExpression
    {
        #region ExempleExpression
        public static ArbreExpression ExempleExpression1()
        {
            return new ArbreExpression()
            {
                Racine = new Addition()
                {
                    Gauche = new Addition()
                    {
                        Gauche = new NoeudValeurEntiere()
                        {
                            Valeur = 2
                        },
                        Droit = new Multiplication()
                        {
                            Gauche = new NoeudValeurEntiere()
                            {
                                Valeur = 3
                            },
                            Droit = new NoeudValeurEntiere()
                            {
                                Valeur = 4
                            }
                        }
                    },
                    Droit = new NoeudValeurEntiere()
                    {
                        Valeur = 5,
                    }
                }
            };
        }
        public static ArbreExpression ExempleExpression2()
        {
            return new ArbreExpression()
            {
                Racine = new Soustraction()
                {
                    Gauche = new Addition()
                    {
                        Gauche = new Multiplication()
                        {
                            Gauche = new NoeudValeurEntiere()
                            {
                                Valeur = 42
                            },
                            Droit = new NoeudValeurEntiere()
                            {
                                Valeur = 3
                            }
                        },
                        Droit = new NoeudValeurEntiere()
                        {
                            Valeur = 17
                        }
                    },
                    Droit = new Division()
                    {
                        Gauche = new NoeudValeurEntiere()
                        {
                            Valeur = 23
                        },
                        Droit = new NoeudValeurEntiere()
                        {
                            Valeur = 7
                        }
                    }
                }
            };
        }
        #endregion
        #region Generer Expression Valide
        private static NoeudArbreExpression ConvertirChaine(string p_chaine)
        {
            if (p_chaine == null)
            {
                throw new ArgumentException(nameof(p_chaine));
            }

            NoeudArbreExpression noeud;

            if (int.TryParse(p_chaine, out int entier))
            {
                noeud = new NoeudValeurEntiere() { Valeur = entier };
            }
            else
            {
                switch (p_chaine)
                {
                    case "+":
                        noeud = new Addition();
                        break;
                    case "-":
                        noeud = new Soustraction();
                        break;
                    case "*":
                        noeud = new Multiplication();
                        break;
                    case "/":
                        noeud = new Division();
                        break;
                    default:
                        throw new InvalidOperationException(nameof(p_chaine));
                }
            }

            return noeud;
        }
        #region Premiere Methode
        private static NoeudArbreExpression ParcoursPrefixe(NoeudArbreExpression p_noeud, List<string> p_valeurs)
        {
            if (p_noeud == null && p_valeurs.Count > 0)
            {
                p_noeud = ConvertirChaine(p_valeurs[0]);
                p_valeurs.RemoveAt(0);

                if (p_noeud is NoeudOperateur)
                {
                    NoeudOperateur noeudOperateur = p_noeud as NoeudOperateur;
                    noeudOperateur.Gauche = ParcoursPrefixe(noeudOperateur.Gauche, p_valeurs);
                    noeudOperateur.Droit = ParcoursPrefixe(noeudOperateur.Droit, p_valeurs);
                }

            }
            return p_noeud;
        }

        public static ArbreExpression GenererArbreExpressionPrefixeValide(string p_prefixe)
        {
            if (p_prefixe == null || p_prefixe.Length == 0)
            {
                throw new ArgumentException(nameof(p_prefixe));
            }

            ArbreExpression arbreExpression = new ArbreExpression();
            List<string> noeudsExpression = p_prefixe.Trim().Split(" ", StringSplitOptions.RemoveEmptyEntries).ToList();

            arbreExpression.Racine = ParcoursPrefixe(arbreExpression.Racine, noeudsExpression);

            return arbreExpression;
        }
        #endregion
        #region Deuxième Méthode
        private static (NoeudArbreExpression, List<string>) ParcoursPrefixe2(List<string> p_valeurs)
        {
            if (p_valeurs.Count > 0 && ConvertirChaine(p_valeurs[0]) is NoeudOperateur)
            {
                NoeudOperateur noeudOperateur = (NoeudOperateur)ConvertirChaine(p_valeurs[0]);
                p_valeurs.RemoveAt(0);

                (noeudOperateur.Gauche, p_valeurs) = ParcoursPrefixe2(p_valeurs);
                (noeudOperateur.Droit, p_valeurs) = ParcoursPrefixe2(p_valeurs);

                return (noeudOperateur, p_valeurs);
            }

            if (p_valeurs.Count > 0 && ConvertirChaine(p_valeurs[0]) is NoeudValeurEntiere)
            {
                NoeudValeurEntiere noeudValeurEntiere = (NoeudValeurEntiere)ConvertirChaine(p_valeurs[0]);
                p_valeurs.RemoveAt(0);

                return (noeudValeurEntiere, p_valeurs);
            }

            return (null, p_valeurs);
        }
        public static ArbreExpression GenererArbreExpressionPrefixeValide2(string p_prefixe)
        {
            if (p_prefixe == null || p_prefixe.Length == 0)
            {
                throw new ArgumentException(nameof(p_prefixe));
            }

            ArbreExpression arbreExpression = new ArbreExpression();
            List<string> noeudsExpression = p_prefixe.Trim().Split(" ", StringSplitOptions.RemoveEmptyEntries).ToList();

            (arbreExpression.Racine, noeudsExpression) = ParcoursPrefixe2(noeudsExpression);

            return arbreExpression;
        }
        #endregion
        #region Troisieme Methode
        public static (List<string>, NoeudArbreExpression) ParcoursPrefixe3(List<string> p_valeurs)
        {
            if (p_valeurs.Count > 0)
            {
                NoeudArbreExpression noeud = ConvertirChaine(p_valeurs[0]);
                p_valeurs.RemoveAt(0);

                if (noeud is NoeudOperateur)
                {
                    NoeudOperateur noeudOperateur = noeud as NoeudOperateur;

                    (p_valeurs, noeudOperateur.Gauche) = ParcoursPrefixe3(p_valeurs);
                    (p_valeurs, noeudOperateur.Droit) = ParcoursPrefixe3(p_valeurs);

                    return (p_valeurs, noeudOperateur);
                }

                return (p_valeurs, noeud);
            }

            return (p_valeurs, null);
        }
        public static ArbreExpression GenererArbreExpressionPrefixeValide3(string p_prefixe)
        {
            if(p_prefixe == null)
            {
                throw new ArgumentNullException(nameof(p_prefixe));
            }

            ArbreExpression arbreExpression = new ArbreExpression();
            List<string> noeudsExpression = p_prefixe.Trim().Split(" ", StringSplitOptions.RemoveEmptyEntries).ToList();

            (noeudsExpression, arbreExpression.Racine) = ParcoursPrefixe3(noeudsExpression);

            return arbreExpression;
        }
        #endregion
        #endregion
    }
}
