﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreNaire_Library
{
    public interface NoeudArbreExpression
    {
        public int Calculer();
    }
}
