﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreNaire_Library
{
    public class Addition : NoeudOperateur
    {
        public override Func<int, int ,int> Valeur => (a,b) => a + b;
    }
}
